# README #

### Original Code Authors ###

https://code.google.com/p/awsgroovyclouds/

https://code.google.com/u/105339729393800384553/
https://code.google.com/u/109574398655460722803/

### What Am I ###
Java and Groovy projects can get started using S3, SQS, and SDB with a simple interface to AWS.

Support for the latest versions of the API including latest SimpleDB BatchPutAttributes, up-to-date Signature Versions - Thread-safe drivers for accessing different AWS accounts - Introduction of concept of namespace to manage different developers accessing same AWS account, different AWS accounts - Easy integration for Java codebases - Easy configurable HTTP, HTTPS requests


### Code license ###

MIT License
